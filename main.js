var canvas,ctx,isdown=false,
    prex=0,
    curx=0,
    prey=0,
    cury=0,
    dot_flag=false;
var penl=Number(document.getElementById("penw").value);
var eral=Number(document.getElementById("penw").value)+8;
var x="black",
    lineW=(writing)?penl:(erasing)?eral:penl;
var writing=false;
var erasing=false;
var circling=false;
var texting=false;
var undoing=false;
var rectangling=false;
var triangling=false;
var fillcircling=false;
var colorfuling=false;
var uploading=false;
var istyping=false;
var lining=false;
var ovaling=false;
var isfill=false;
var isran=false;
var isupload=false;
var box;
var issnoopy=true;
var tmpfont="serif";
var tmpsize=18;
var tmpbtn=document.getElementById("penbtn");
var tmpbtn2=document.getElementById("erabtn");
var tmpbtn3=document.getElementById("cirbtn");
var tmpbtn4=document.getElementById("txtbtn");
var tmpbtn5=document.getElementById("recbtn");
var tmpbtn6=document.getElementById("tribtn");
var tmpbtn7=document.getElementById("linebtn");
var tmpbtn8=document.getElementById("ovalbtn");
var tmpbtn9=document.getElementById("undbtn");
var tmpbtn10=document.getElementById("redbtn");
var tmpbtn11=document.getElementById("fillcir");
var tmpbtn12=document.getElementById("clbtn");
var w,h;
var drawstack=new Array();
var redostack=new Array();
var cStep;
var reStep;
const realFileBtn=document.getElementById("real-file");
const customBtn=document.getElementById("ulbtn");
const customTxt=document.getElementById("custom-text");
customBtn.addEventListener("click",function(){
    realFileBtn.click();
});
function init(){
    
    canvas=document.getElementById('myCanvas');
    ctx=canvas.getContext("2d");
    w=canvas.width;
    h=canvas.height;
    canvas.addEventListener("mousemove",function(e){
        findxy('move',e);
        
    },false);
    canvas.addEventListener("mousedown",function(e){
        findxy('down',e)
    },false);
    canvas.addEventListener("mouseup",function(e){
        findxy('up',e)
    },false);
    canvas.addEventListener("mouseout",function(e){
        findxy('out',e)
    },false);
    window.addEventListener("keypress",function(e){
        key('keydown',e)
    },false);
    cStep=-1;
    reStep=-1;
    cPush();
    document.getElementById("mute").style.visibility="hidden";
}

function draw(){
    if(writing||erasing){
        ctx.beginPath();
        ctx.moveTo(prex,prey);
        ctx.lineTo(curx,cury);
        ctx.lineCap='round';
        var f_color=document.getElementById("font_color").value;
        
        if(writing) {
            if(!isran){
                ctx.strokeStyle=(colorfuling)?chooseColor(curx):f_color;
            }
            else{
               ctx.strokeStyle=(colorfuling)?getRandomColor():f_color;
            }
        }
        else ctx.strokeStyle=f_color;
        var tmp=document.getElementById("penw").value;
        //lineW=Number(tmp);
        var penl=Number(document.getElementById("penw").value);
        var eral=Number(document.getElementById("penw").value)+15;
        lineW=(writing)?penl:(erasing)?eral:penl;
        ctx.lineWidth=lineW;
        
        ctx.stroke();
        ctx.closePath();
    }
    
}
function cir(){
    ctx.beginPath();
    var cirl=Number(document.getElementById("penw").value);
    ctx.lineWidth=cirl;
    var f_color=document.getElementById("font_color").value;
    if(!isran){
        ctx.strokeStyle=(colorfuling)?chooseColor(curx):f_color;
    }
    else{
        ctx.strokeStyle=(colorfuling)?getRandomColor():f_color;
    }
    var dis=Math.sqrt((curx-prex)**2+(cury-prey)**2);
    var midx=(curx+prex)/2;
    var midy=(cury+prey)/2;
    ctx.arc(midx, midy, dis/2, 0, 2*Math.PI);
    ctx.fillStyle=(colorfuling)?chooseColor(curx):document.getElementById("font_color").value;
    var flag1=Math.abs(prex-curx)<3;
    var flag2=Math.abs(prey-cury)<3;
    if(flag1&&flag2)ctx.fill();
    if(fillcircling) ctx.fill();
    //if(document.getElementById("fill").checked) ctx.fill();
    ctx.stroke();
}
function rec(){
    ctx.beginPath();
    var cirl=Number(document.getElementById("penw").value);
    ctx.lineWidth=cirl;
    var f_color=document.getElementById("font_color").value;
    if(!isran){
        ctx.strokeStyle=(colorfuling)?chooseColor(curx):f_color;
    }
    else{
       ctx.strokeStyle=(colorfuling)?getRandomColor():f_color;
    }
    //var f=document.getElementById("fill").checked;
    var f=false;
    ctx.fillStyle=(colorfuling)?chooseColor(curx):document.getElementById("font_color").value;
    if(curx>prex&&cury>prey) {
        if(!fillcircling)ctx.strokeRect(prex,prey,curx-prex,cury-prey);
        else ctx.fillRect(prex,prey,curx-prex,cury-prey);
    }
    else if(curx>prex&&cury<prey) {
        if(!fillcircling) ctx.strokeRect(prex,cury,curx-prex,prey-cury);
        else ctx.fillRect(prex,cury,curx-prex,prey-cury);
    }
    else if(curx<prex&&cury>prey) {
        if(!fillcircling) ctx.strokeRect(curx,prey,prex-curx,cury-prey);
        else ctx.fillRect(curx,prey,prex-curx,cury-prey);
    }
    else if(curx<prex&&cury<prey) {
        if(!fillcircling) ctx.strokeRect(curx,cury,prex-curx,prey-cury);
        else ctx.fillRect(curx,cury,prex-curx,prey-cury);
    }
    
    
}
function tri(){
    ctx.beginPath();
    var tril=Number(document.getElementById("penw").value);
    ctx.lineWidth=tril;
    var f_color=document.getElementById("font_color").value;
    if(!isran){
        ctx.strokeStyle=(colorfuling)?chooseColor(curx):f_color;
    }
    else{
       ctx.strokeStyle=(colorfuling)?getRandomColor():f_color;
    }
    var dis=Math.abs(curx-prex)/2;
    if(curx>prex&&cury>prey){
    ctx.moveTo(curx,cury);
    ctx.lineTo(prex,cury);
    ctx.lineTo(prex+dis,prey);
    }
    else if(curx>prex&&cury<prey){
        ctx.moveTo(prex,prey);
        ctx.lineTo(curx,prey);
        ctx.lineTo(prex+dis,cury);
    }
    else if(curx<prex&&cury>prey){
        ctx.moveTo(curx,cury);
        ctx.lineTo(prex,cury);
        ctx.lineTo(curx+dis,prey);
    }
    else if(curx<prex&&cury<prey){
        ctx.moveTo(prex,prey);
        ctx.lineTo(curx,prey);
        ctx.lineTo(curx+dis,cury);

    }
    ctx.closePath();
    ctx.fillStyle=(colorfuling)?chooseColor(curx):document.getElementById("font_color").value;
    if(fillcircling) ctx.fill();
    //if(document.getElementById("fill").checked) ctx.fill();
    ctx.stroke();
}
function li(){
    ctx.beginPath();
    ctx.moveTo(prex,prey);
    ctx.lineTo(curx,cury);
    ctx.lineCap='round';
    var f_color=document.getElementById("font_color").value;
    if(!isran){
        ctx.strokeStyle=(colorfuling)?chooseColor(curx):f_color;
    }
    else{
       ctx.strokeStyle=(colorfuling)?getRandomColor():f_color;
    }
    var tmp=document.getElementById("penw").value;
    ctx.lineWidth=tmp;
    
    ctx.stroke();
    ctx.closePath();

}
function ov(){
    ctx.beginPath();
    var cx=(curx+prex)/2;
    var cy=(cury+prey)/2;
    ctx.lineCap='round';
    var f_color=document.getElementById("font_color").value;
    if(!isran){
        ctx.strokeStyle=(colorfuling)?chooseColor(curx):f_color;
    }
    else{
       ctx.strokeStyle=(colorfuling)?getRandomColor():f_color;
    }
    var tmp=document.getElementById("penw").value;
    ctx.lineWidth=tmp;
    var dx=Math.abs(curx-prex);
    var dy=Math.abs(cury-prey);
    if(curx>prex&&cury>prey) ctx.ellipse(cx, cy, dx, dy, 0, 0, 2 * Math.PI);
    else if(curx>prex&&cury<prey) ctx.ellipse(cx, cy, dx, dy, 0, 0, 2 * Math.PI);
    else if(curx<prex&&cury>prey) ctx.ellipse(cx, cy, dx, dy, 0, 0, 2 * Math.PI);
    else if(curx<prex&&cury<prey) ctx.ellipse(cx, cy, dx, dy, 0, 0, 2 * Math.PI);
    ctx.fillStyle=(colorfuling)?chooseColor(curx):document.getElementById("font_color").value;
    if(fillcircling) ctx.fill();
    ctx.stroke();

}
function erase(){
    ctx.beginPath();
    ctx.arc(prex,prey,8,0,Math.PI*2,false);
    ctx.fill();
    prex=curx;
    prey=cury;
}
function findxy(res,e){
    if(res=='down'){
        var tmpmute=(document.getElementById("mute").style.visibility=="hidden");
        if(tmpmute)document.getElementById("happyaudio").play();
        var tmptype=texting&&istyping;
        if(!(circling||rectangling||triangling||lining||ovaling)){
            if(!tmptype){
                prex=curx;
                prey=cury;
            }
            
        }
        curx=e.clientX-canvas.offsetLeft;
        cury=e.clientY-canvas.offsetTop;
        if(texting&&!istyping) {
            starttype();
            istyping=true;
        }
        else if(texting&&istyping){
            endtype();
            istyping=false;
        }
        isdown=true;
        dot_flag=true;
        if(dot_flag&&writing){
            ctx.beginPath();
            ctx.fillStyle=x;
            ctx.fillRect(curx,cury,2,2);
            ctx.closePath();
            dot_flag=false;
        }
    }
    if(res=='up'||res=="out"){
        isdown=false;

        if(circling&&res=='up') {
            curx=e.clientX-canvas.offsetLeft;
            cury=e.clientY-canvas.offsetTop;
            cir();
        }
        if(rectangling&&res=='up') {
            curx=e.clientX-canvas.offsetLeft;
            cury=e.clientY-canvas.offsetTop;
            rec();
        }
        if(triangling&&res=='up') {
            curx=e.clientX-canvas.offsetLeft;
            cury=e.clientY-canvas.offsetTop;
            tri();
        }
        if(texting&&istyping&&res=='up'){
            curx=e.clientX-canvas.offsetLeft;
            cury=e.clientY-canvas.offsetTop;
        }
        if(lining&&res=='up') {
            curx=e.clientX-canvas.offsetLeft;
            cury=e.clientY-canvas.offsetTop;
            li();
        }
        if(ovaling&&res=='up'){
            curx=e.clientX-canvas.offsetLeft;
            cury=e.clientY-canvas.offsetTop;
            ov();
        }
        //var working=(writing||erasing||circling||texting||rectangling||uploading)
        if(res=='up') {
            ////console.log("mup");
            cPush();
        }
        
    }
    var flag1=(e.clientX>canvas.offsetLeft)&&(e.clientX<canvas.offsetLeft+w)&&(e.clientY>canvas.offsetTop)&&(e.clientY<canvas.offsetTop+h);
    if(res=='out'&&(!flag1)&&texting&&istyping){
            //console.log("out");
            istyping=false;
            endtype();

        }
    if(res=='move'){
        if(isdown&&(writing||erasing)){
            prex=curx;
            prey=cury;
            curx=e.clientX-canvas.offsetLeft;
            cury=e.clientY-canvas.offsetTop;
            draw();
        }
        if(isdown&&(circling||rectangling||triangling||lining||ovaling)){
            curx=e.clientX-canvas.offsetLeft;
            cury=e.clientY-canvas.offsetTop;
            if(circling) {
                ctx.putImageData(drawstack[cStep],0,0);
                cir();
            }
            else if(rectangling){
                ctx.putImageData(drawstack[cStep],0,0);
                rec();
            }
            else if(triangling){
                ctx.putImageData(drawstack[cStep],0,0);
                tri();
            }
            else if(lining){
                ctx.putImageData(drawstack[cStep],0,0);
                li();
            }
            else if(ovaling){
                ctx.putImageData(drawstack[cStep],0,0);
                ov();
            }
        }
        if(!isdown&&(circling||rectangling||triangling||lining||ovaling)){
            prex=curx;
            prey=cury;
            curx=e.clientX-canvas.offsetLeft;
            cury=e.clientY-canvas.offsetTop;
        }
        if(texting){
            
            curx=e.clientX-canvas.offsetLeft;
            cury=e.clientY-canvas.offsetTop;
        }
        if(uploading) uploading=false;
        if(isupload){
            cPush();
            isupload=false;
        }
    }
}
function key(res,e){
    if(res=='keydown'&&texting&&istyping&&e.key=="Enter"){
        typing=false;
        endtype();
    }
}
function pen(){
    document.getElementById("clickaudio").play();
    writing=!writing;
    if(writing) {
        erasing=false;
        circling=false;
        texting=false;
        rectangling=false;
        triangling=false;
        lining=false;
        ovaling=false;
        change_cursor("pen");
        ctx.globalCompositeOperation="source-over";
        tmpbtn.style.backgroundColor="gray";
        tmpbtn2.style.backgroundColor="white";
        tmpbtn3.style.backgroundColor="white";
        tmpbtn4.style.backgroundColor="white";
        tmpbtn5.style.backgroundColor="white";
        tmpbtn6.style.backgroundColor="white";
        tmpbtn7.style.backgroundColor="white";
        tmpbtn8.style.backgroundColor="white";
        tmpbtn9.style.backgroundColor="white";
        tmpbtn10.style.backgroundColor="white";
    }
    else if(!writing){
        tmpbtn.style.backgroundColor="white";
        change_cursor("de");
    }
}
function eraser(){
    document.getElementById("clickaudio").play();
    erasing=!erasing;
    if(erasing) {
        change_cursor("era");
        writing=false;
        circling=false;
        texting=false;
        rectangling=false;
        triangling=false;
        lining=false;
        ovaling=false;
        ctx.globalCompositeOperation="destination-out";
        tmpbtn.style.backgroundColor="white";
        tmpbtn2.style.backgroundColor="gray";
        tmpbtn3.style.backgroundColor="white";
        tmpbtn4.style.backgroundColor="white";
        tmpbtn5.style.backgroundColor="white";
        tmpbtn6.style.backgroundColor="white";
        tmpbtn7.style.backgroundColor="white";
        tmpbtn8.style.backgroundColor="white";
        tmpbtn9.style.backgroundColor="white";
        tmpbtn10.style.backgroundColor="white";
        
    }
    else if(!erasing){
        tmpbtn2.style.backgroundColor="white";
        change_cursor("de");

    }
}
function circle(){
    document.getElementById("clickaudio").play();
    circling=!circling;
    if(circling){
        change_cursor("cir");
        writing=false;
        erasing=false;
        texting=false;
        rectangling=false;
        triangling=false;
        lining=false;
        ovaling=false;
        ctx.globalCompositeOperation="source-over";
        tmpbtn.style.backgroundColor="white";
        tmpbtn2.style.backgroundColor="white";
        tmpbtn3.style.backgroundColor="gray";
        tmpbtn4.style.backgroundColor="white";
        tmpbtn5.style.backgroundColor="white";
        tmpbtn6.style.backgroundColor="white";
        tmpbtn7.style.backgroundColor="white";
        tmpbtn8.style.backgroundColor="white";
        tmpbtn9.style.backgroundColor="white";
        tmpbtn10.style.backgroundColor="white";
    }
    else if(!circling){
        tmpbtn3.style.backgroundColor="white";
        change_cursor("de");
    }
}
function text(){
    document.getElementById("clickaudio").play();
    texting=!texting;
    if(texting){
        istyping=false;
        change_cursor("txt");
        writing=false;
        erasing=false;
        circling=false;
        rectangling=false;
        triangling=false;
        lining=false;
        ovaling=false;
        ctx.globalCompositeOperation="source-over";
        tmpbtn.style.backgroundColor="white";
        tmpbtn2.style.backgroundColor="white";
        tmpbtn3.style.backgroundColor="white";
        tmpbtn4.style.backgroundColor="gray";
        tmpbtn5.style.backgroundColor="white";
        tmpbtn6.style.backgroundColor="white";
        tmpbtn7.style.backgroundColor="white";
        tmpbtn8.style.backgroundColor="white";
        tmpbtn9.style.backgroundColor="white";
        tmpbtn10.style.backgroundColor="white";
    }
    else if(!texting){
        istyping=false;
        tmpbtn4.style.backgroundColor="white";
        change_cursor("de");
    }
}
function rectangle(){
    document.getElementById("clickaudio").play();
    rectangling=!rectangling;
    if(rectangling){
        change_cursor("rec");
        writing=false;
        erasing=false;
        circling=false;
        texting=false;
        triangling=false;
        lining=false;
        ovaling=false;
        ctx.globalCompositeOperation="source-over";
        tmpbtn.style.backgroundColor="white";
        tmpbtn2.style.backgroundColor="white";
        tmpbtn3.style.backgroundColor="white";
        tmpbtn4.style.backgroundColor="white";
        tmpbtn5.style.backgroundColor="gray";
        tmpbtn6.style.backgroundColor="white";
        tmpbtn7.style.backgroundColor="white";
        tmpbtn8.style.backgroundColor="white";
        tmpbtn9.style.backgroundColor="white";
        tmpbtn10.style.backgroundColor="white";

    }
    else if(!rectangling){
        tmpbtn5.style.backgroundColor="white";
        ////console.log("nope");
        change_cursor("de");
    }
}
function triangle(){
    document.getElementById("clickaudio").play();
    triangling=!triangling;
    if(triangling){
        change_cursor("tri");
        writing=false;
        erasing=false;
        circling=false;
        texting=false;
        rectangling=false;
        lining=false;
        ovaling=false;
        ctx.globalCompositeOperation="source-over";
        tmpbtn.style.backgroundColor="white";
        tmpbtn2.style.backgroundColor="white";
        tmpbtn3.style.backgroundColor="white";
        tmpbtn4.style.backgroundColor="white";
        tmpbtn5.style.backgroundColor="white";
        tmpbtn6.style.backgroundColor="gray";
        tmpbtn7.style.backgroundColor="white";
        tmpbtn8.style.backgroundColor="white";
        tmpbtn9.style.backgroundColor="white";
        tmpbtn10.style.backgroundColor="white";
    }
    else if(!triangling){
        tmpbtn6.style.backgroundColor="white";
        change_cursor("de");
    }
}
function line(){
    document.getElementById("clickaudio").play();
    lining=!lining
    if(lining){
        change_cursor("li");
        writing=false;
        erasing=false;
        circling=false;
        texting=false;
        rectangling=false;
        triangling=false;
        ovaling=false;
        ctx.globalCompositeOperation="source-over";
        tmpbtn.style.backgroundColor="white";
        tmpbtn2.style.backgroundColor="white";
        tmpbtn3.style.backgroundColor="white";
        tmpbtn4.style.backgroundColor="white";
        tmpbtn5.style.backgroundColor="white";
        tmpbtn6.style.backgroundColor="white";
        tmpbtn7.style.backgroundColor="gray";
        tmpbtn8.style.backgroundColor="white";
        tmpbtn9.style.backgroundColor="white";
        tmpbtn10.style.backgroundColor="white";
    }
    else if(!lining){
        tmpbtn7.style.backgroundColor="white";
        change_cursor("de");
    }
}
function oval(){
    document.getElementById("clickaudio").play();
    ovaling=!ovaling
    if(ovaling){
        change_cursor("oval");
        writing=false;
        erasing=false;
        circling=false;
        texting=false;
        rectangling=false;
        triangling=false;
        lining=false;
        ctx.globalCompositeOperation="source-over";
        tmpbtn.style.backgroundColor="white";
        tmpbtn2.style.backgroundColor="white";
        tmpbtn3.style.backgroundColor="white";
        tmpbtn4.style.backgroundColor="white";
        tmpbtn5.style.backgroundColor="white";
        tmpbtn6.style.backgroundColor="white";
        tmpbtn7.style.backgroundColor="white";
        tmpbtn8.style.backgroundColor="gray";
        tmpbtn9.style.backgroundColor="white";
        tmpbtn10.style.backgroundColor="white";
    }
    else if(!ovaling){
        tmpbtn8.style.backgroundColor="white";
        change_cursor("de");
    }

}
function undo(){
    document.getElementById("clickaudio").play();
    writing=false;
    erasing=false;
    circling=false;
    texting=false;
    rectangling=false;
    triangling=false;
    lining=false;
    ovaling=false;
    ctx.globalCompositeOperation="source-over";
    tmpbtn.style.backgroundColor="white";
    tmpbtn2.style.backgroundColor="white";
    tmpbtn3.style.backgroundColor="white";
    tmpbtn4.style.backgroundColor="white";
    tmpbtn5.style.backgroundColor="white";
    tmpbtn6.style.backgroundColor="white";
    tmpbtn7.style.backgroundColor="white";
    tmpbtn8.style.backgroundColor="white";
    tmpbtn9.style.backgroundColor="white";
    tmpbtn10.style.backgroundColor="white";
    cUndo();
    //trying to undo
}
function redo(){
    document.getElementById("clickaudio").play();
    writing=false;
    erasing=false;
    circling=false;
    texting=false;
    rectangling=false;
    triangling=false;
    lining=false;
    ovaling=false;
    ctx.globalCompositeOperation="destination-out";
    tmpbtn.style.backgroundColor="white";
    tmpbtn2.style.backgroundColor="white";
    tmpbtn3.style.backgroundColor="white";
    tmpbtn4.style.backgroundColor="white";
    tmpbtn5.style.backgroundColor="white";
    tmpbtn6.style.backgroundColor="white";
    tmpbtn7.style.backgroundColor="white";
    tmpbtn8.style.backgroundColor="white";
    tmpbtn9.style.backgroundColor="white";
    tmpbtn10.style.backgroundColor="white";
    cRedo();

}
function cPush(){
    cStep++;
    //console.log("push");
    var ctx=canvas.getContext("2d");
    //if(cStep<drawstack.length) {cStep=drawstack.length;}
    var img=document.getElementById("myCanvas");
    drawstack.push(ctx.getImageData(0,0,w,h));
}
function cUndo(){
    var canvas4=document.getElementById("myCanvas");
    var ctx=canvas4.getContext("2d");
    if(cStep>0){
        
        redostack.push(ctx.getImageData(0,0,w,h));
        ctx.putImageData(drawstack[cStep-1],0,0);
        //console.log("new");
        
        reStep++;
        drawstack.pop();
        cStep--;
    }
}
function fillcircle(){
    document.getElementById("clickaudio").play();
    fillcircling=!fillcircling;
    if(circling) change_cursor("cir");
    else if(rectangling) change_cursor("rec");
    else if(triangling) change_cursor("tri");
    else if(ovaling) change_cursor("oval");
    if(fillcircling){
        ctx.globalCompositeOperation="source-over";
        tmpbtn11.style.backgroundColor="gray";
    }
    else if(!fillcircling){
        tmpbtn11.style.backgroundColor="white";
    }
}
function cRedo() {
    var canvas5=document.getElementById("myCanvas");
    var ctx=canvas5.getContext("2d");
    if(reStep>=0){
        //reStep--;
        
        ctx.putImageData(redostack[reStep],0,0);
        drawstack.push(ctx.getImageData(0,0,w,h));
        ////console.log("new");
        cStep++;
        redostack.pop();
        reStep--;
    }
}
function download(){
    var canvas1 = document.getElementById("myCanvas");
    image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
    var link = document.createElement('a');
    link.download = "my_image.png";
    link.href = image;
    link.click();
    
}
function previewImage(input){
    cPush();
    var reader=new FileReader();
    var canvas3=document.getElementById("myCanvas");
    var ctx=canvas.getContext("2d");
    reader.onload=function(event){
        var img=new Image();
        img.onload=function(){
            ctx.drawImage(img,0,0);
        }
        img.src=event.target.result;
    };
    reader.readAsDataURL(input.files[0]);
    //uploading=true;
    cPush();
    isupload=true;
}
function clearall(){
    document.getElementById("clickaudio").play();
    var a=confirm("Are you sure you want to clear?");
    cPush();
    if(a)ctx.clearRect(0, 0, w, h);
    cPush();
}
function change_cursor(nowstyle) {
    if(nowstyle=="pen") canvas.style.cursor="url('./img/smpen.png'),auto";
    else if(nowstyle=="era") canvas.style.cursor="url('./img/smeraser.png'),auto";
    else if(nowstyle=="cir") {
        if(!fillcircling) canvas.style.cursor="url('./img/smcircle.png'),auto";
        else canvas.style.cursor="url('./img/smfillcircle.PNG'),auto";
    }
    else if(nowstyle=="txt") canvas.style.cursor="url('./img/smtext.png'),auto";
    else if(nowstyle=="rec") {
        if(!fillcircling)canvas.style.cursor="url('./img/smrectangle.png'),auto";
        else canvas.style.cursor="url('./img/smfillrec.png'),auto";
    }
    else if(nowstyle=="tri") {
        if(!fillcircling) canvas.style.cursor="url('./img/smtriangle.PNG'),auto";
        else canvas.style.cursor="url('./img/smfilltri.png'),auto";
    }
    else if(nowstyle=="li") canvas.style.cursor="url('./img/smline.PNG'),auto";
    else if(nowstyle=="oval") {
        if(!fillcircling) canvas.style.cursor="url('./img/smoval.PNG'),auto";
        else canvas.style.cursor="url('./img/smfilloval.PNG'),auto";
    }
    else if(nowstyle=="de"){
        canvas.style.cursor="context-menu";

    }
    
}
function penflying(){
    var myp=document.getElementById('smpen');
    if (!myp) {
        return;
    }
    var intX=window.event.clientX;
    var intY=window.event.clientY;
    myp.style.left=intX+"px";
    myp.style.top=intY+"px";
}
function starttype(){
    box = document.createElement('input');
    box.style.height="50px";
    //box.setAttribute("style","height:100px");
    /*if(tmpsize==100) {
        box.size.height="100px";
    }*/
    box.style="text";
    box.style.position = 'absolute';
    box.style.background="transparent";
    box.style.left=curx+canvas.offsetLeft+'px';
    box.style.top=cury+canvas.offsetTop+'px';
    document.body.appendChild(box);
}
function endtype(){
    cPush();
    var word=box.value;
    box.style.visibility="hidden";
    ctx.font = tmpsize+'px '+tmpfont;
    ctx.fillStyle=(colorfuling)?chooseColor(curx):document.getElementById("font_color").value;
    //ctx.font = '20px Georgia';
    ctx.fillText(word,prex,prey);
}
var changeSize = function(size){
    tmpsize = Number(size.value);
  }
var changeFont = function(font){
  tmpfont = font.value;
}

var textWrapper = document.querySelector('.ml2');
textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

anime.timeline({loop: false})
  .add({
    targets: '.ml2 .letter',
    scale: [4,1],
    opacity: [0,1],
    translateZ: 0,
    easing: "easeOutExpo",
    duration: 950,
    delay: (el, i) => 70*i
  }).add({
    targets: '.ml2',
    opacity: 1,
    duration: 1000,
    easing: "easeOutExpo",
    delay: 1000
  });
function snoopy(){
    var snp=document.getElementById("snoopy");
    if(issnoopy){
        issnoopy=false;
        snp.style.visibility="hidden";
    }
    else{
        issnoopy=true;
        snp.style.visibility="visible";
    }
}
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
function chooseColor(x){
    var color;
    if(x>0&&x<100) color="red";
    else if(x>=100&&x<200) color="orange";
    else if(x>=200&&x<300) color="yellow";
    else if(x>=300&&x<400) color="yellowgreen";
    else if(x>=400&&x<500) color="green";
    else if(x>=500&&x<600) color="rgb(0,200,200)";
    else if(x>=600&&x<700) color="blue";
    else if(x>=700&&x<800) color="purple";
    else if(x>=800&&x<900) color="gray";
    else color="black"; 
    return color;
}
function colorful(){
    colorfuling=!colorfuling;
    if(colorfuling){
        ctx.globalCompositeOperation="source-over";
        tmpbtn12.style.backgroundColor="gray";
    }
    else if(!colorfuling){
        tmpbtn12.style.backgroundColor="white";
    }

}
function isRandom(){
    var checkBox = document.getElementById("ranCheck");
    // If the checkbox is checked, display the output text
    if (checkBox.checked == true){
      isran=true;
    } else {
      isran=false;
    }
    //console.log("in");
}
function muting(){
    var mysound=document.getElementById("sound");
    var mymute=document.getElementById("mute");
    mysound.style.visibility="hidden";
    mymute.style.visibility="visible";
    document.getElementById("happyaudio").pause();
}
function playmuc(){
    var mysound=document.getElementById("sound");
    var mymute=document.getElementById("mute");
    mysound.style.visibility="visible";
    mymute.style.visibility="hidden";
    document.getElementById("happyaudio").play();
}
