# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | Y         |


---

### How to use 

    Describe how to use your web and maybe insert images to help you explain.
    從畫面右邊可以看到共有16個buttons, 以下一一做說明
![](https://i.imgur.com/BBvuluD.png)


    1.Color
            最上面有一個選顏色的按鈕, 點開可以進行選顏色的動作, 這個顏色套用到幾乎所有其他功
        能,包括接下來的畫筆、圓圈、打字、矩形、三角形... 舉凡有顏色的大致都是這裡所選出的色。
![](https://i.imgur.com/08mHHAN.png)



    2.Pen 
            畫筆功能, 可調顏色與粗細
![](https://i.imgur.com/QzuiRTh.png)


    3.Eraser
            橡皮擦功能, 可擦掉目前在畫面上的所有筆跡或圖片
![](https://i.imgur.com/Kf3RiMx.png)

        
    4.Circle
            滑鼠左鍵壓下開始畫圓, 放開結束畫圓
![](https://i.imgur.com/hUQz4a8.png)

        
    5.Text
            打字功能,點選畫布開始畫, 可透過下面點選決定字體與大小, 一旦游標按下enter或是
        canvas的其他地方又或是游標移出canvas的範圍便會結束打字
![](https://i.imgur.com/0jT5xEt.png)

        
    6.rectangle
            滑鼠左鍵壓下開始畫長方形, 放開結束畫的動作
![](https://i.imgur.com/yokByZJ.png)

    7.triangle
            滑鼠左鍵壓下開始畫三角形, 放開結束畫的動作
![](https://i.imgur.com/OsKCt3K.png)

        
    8.line
            滑鼠左鍵壓下開始畫直線, 放開結束畫直線
![](https://i.imgur.com/Fe45a7K.png)

        
    9.oval
            滑鼠左鍵壓下開始畫橢圓, 放開結束畫的動作
![](https://i.imgur.com/Un0Ryjz.png)

    10.undo
            滑鼠左鍵點擊undo
![](https://i.imgur.com/pq2gLBx.png)

        
    11.redo
            滑鼠左鍵點擊redo
![](https://i.imgur.com/AXcSwfe.png)

        
    12.fill
            點下表示目前的圓形、矩形、三角形、橢圓畫下去都會是填滿的方式呈現, 顏色一樣是上面
        的顏色選擇所決定
![](https://i.imgur.com/SDantNs.png)

        
    13.clear
            點下清空當前畫布(可undo)
![](https://i.imgur.com/4HMRUOR.png)


        
    14.download
            滑鼠左鍵點擊下載畫布圖片

![](https://i.imgur.com/xZ1uv8C.png)

    15.upload
            滑鼠左鍵點擊上傳圖片
![](https://i.imgur.com/622jpYF.png)

        
    16.colorful
            點下表示有顏色的功能在畫布上會呈現彩色, 這個模式是固定的, 對於畫筆而言出現在何處
        便會呈現該顏色; 而對於直線及其他圖形而言, 則是最後游標停在哪裡該圖形便會出現那個顏色。
        
##### 其他功能
![](https://i.imgur.com/ewShGjN.png)

    1.Random Color
            點選起來之後當我們在用上面的colorful之時, 取顏色的方式改成以隨機的方式, 也就是
        說直到畫完前無法預期畫筆的顏色或是圖形最終的顏色
![](https://i.imgur.com/br1wCRL.png)

     
    2.Tool Width
            可以以此調整畫筆大小與圖形邊框大小
![](https://i.imgur.com/93CrZyW.png)

            
    3.Font and Size
            下方我們可以看到有兩個可以選擇的東西, 一個是字體而另一個是字的大小, 共有十種字體
        與九種大小可以選擇
![](https://i.imgur.com/Ac5Yza0.gif)



        
    4.Gif
            純粹為一個小巧思所以放了一個 Snoopy 的gif讓整體不顯地那麼單調, 若使用者覺得
        了厭煩又或是會被其干擾, 則可點及gif本身或下方紅色小房子以讓它消失, 若要再顯示
        則點擊房子即可
![](https://i.imgur.com/m7Nlbd0.png) ![](https://i.imgur.com/E7KZSC2.png)

    5.Music
            當使用者開始畫(使用功能)的時候, 音樂便會開始播放, 同樣的若是覺得音樂煩躁可以點擊
        螢幕左下方的喇叭, 讓音樂停止; 若是我們要再次播放則再點擊右下角靜音喇叭以開始播放
 ![](https://i.imgur.com/oGYQiM3.png)
       
    6.Button sound
            當使用者點擊右方按鍵之時會發出按下的聲音
            
    7.Cursor
            使用者在進行某一項功能之時, 游標便會轉變成那個形狀。 比如畫筆游標就會變成畫筆、 
        空心的圓形便會使游標變為空心圓、實心圓狀態就會使游標變實心圓
        
    8.Title effect
            當使用者進入頁面的時候上方的 "My canvas" 這幾個字便會依次從左到右顯示出來
![canvas img](https://upload.cc/i1/2020/04/11/jaLTfs.png
)
        
        

### Function description

    Decribe your bouns function and how to use it.
    以上是我做的bonus
    1.Oval shape 
            看網路上畫橢圓的方式自己填入參數並且按照圓形及長方形的方式進行繪製
            
    2.Colorful
            點下使功能顏色呈現彩色的狀態, 若是普通狀態, 在畫布上的顏色設定是固定的, 以x軸決
        定, 從左到右大致會是彩虹的順序(紅橙黃綠藍靛紫)。 若今天是Random color的狀態, 則我
        有一個function可以用random 0~F 以隨機產生顏色。 
            在每一次寫或畫圖形移動的時候便會呼叫這個Random Color的function去取顏色值, 這
        樣就可以達到隨機顏色的目的了。 對於畫筆而言它是一直畫一直產生不同顏色, 而對於各種圖形
        而言則是停下的那點決定其顏色
        
    3.Audio
            使用audio與onclick的功能決定是否播放音樂或音效(包括主頁面的音樂以及buttons 
        按下去的小音效)
            

### Gitlab page link

     "https://107062314.gitlab.io/AS_01_WebCanvas"

### Others (Optional)

        我認為這次的lab算滿好玩的, 雖說毫無疑問地對我這個理解能力較差的人而言,花在上面的時間
    真的不少, 做這個網站要用到的東西也幾乎都沒有從課堂上學到或去深究。
    
        一開始著手進行此作業的時候覺得有點害怕, 害怕不會做或是做不完, 但最後我發現其實只要
    不斷地上網查資料便能步步接近目標。 雖說是查資料,但其實常常得到的資源卻是沒有效果, 如此便
    要不斷試到成功為止。
    
        從最初的微恐懼到最終的覺得有趣, 我相信這就是學習的一個過程, 另外我這次還學到一個很重要
    的技能, 那就是查資料的能力。 做為一個資工系的學生, 相信知識並沒有學完的一天, 且資訊日新月
    異, 不斷地在改變, 自學的能力仍是非常重要的。
        
        
        

<style>
table th{
    width: 100%;
}
</style>